<?php

namespace Drupal\entity_id_manager\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_id_manager\EntityIdManager;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\ContentEntityInterface;

class EntityIdCommands extends DrushCommands
{
  const NUM_ID_NEW_OFFER = 25;

  protected EntityIdManager $entityIdManager;
  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(EntityIdManager $entity_id_manager, EntityTypeManagerInterface $entity_type_manager)
  {
    $this->entityIdManager = $entity_id_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @hook interact @entity-type
   */
  public function interactEntityType($input)
  {
    if (empty($input->getArgument('entity_type_id'))) {
      $definitions = $this->entityTypeManager->getDefinitions();
      $definitions = array_filter($definitions, function ($def) {
        return ($def instanceof ContentEntityType);
      });
      $options = array_map(function ($def) {
        return $def->id();
      }, $definitions);

      $choice = $this->io()->choice('Entity Type', $options);
      $input->setArgument('entity_type_id', $choice);
    }
  }

  protected static function resolveLabel(ContentEntityInterface $entity): string
  {
    switch ($entity->getEntityTypeId()) {
      case 'commerce_product_variation':
        return $entity->getSku();
      default:
        return $entity->label();
    }
  }

  /**
   * @hook interact @id-old
   */
  public function interactId($input): void
  {
    if (empty($input->getArgument('id_old'))) {
      $entity_type_id = $input->getArgument('entity_type_id');
      $storage = $this->entityTypeManager->getStorage($entity_type_id);

      $id_column_name = $this->entityTypeManager->getDefinition($entity_type_id)->getKey('id');
      $ids = $storage->getQuery()->accessCheck(FALSE)->sort($id_column_name)->execute();

      if (empty($ids)) {
        $this->logger()->warning('Entity tables empty -> Nothing to change of the id of.');
        return;
      }

      $this->logger()->notice("Working with " . $storage::class);

      foreach ($ids as $id) {
        if ($entity = $storage->load($id))

        $bundle = $entity->bundle();
        $label = self::resolveLabel($entity);
        // @see https://drupal.stackexchange.com/questions/187980/how-to-get-bundle-label-from-entity
        $options[$id] = "(id: $id, bundle: $bundle) $label";
      }

      $choice = $this->io()->choice($storage->getEntityType()->getLabel(), $options);
      $input->setArgument('id_old', $choice);
    }
  }

  /**
   * @hook interact @id-new
   */
  public function interactNewId($input): void
  {
    if (empty($input->getArgument('id_new'))) {
      $available_ids = $this->entityIdManager->getAvailableIds($input->getArgument('entity_type_id'));

      $options = array_combine(
        $available_ids,
        array_map(function(int $id): string { return "(id: $id)";}, $available_ids)
      );

      $choice = $this->io()->choice('New Entity id', $options);
      $input->setArgument('id_new', $choice);
    }
  }

  /**
   * Changes the id of an entity
   *
   * @entity-type
   * @param string $entity_type_id
   *   Entity_type
   * @id-old
   * @param int $id_old
   *   Entity-id.
   * @id-new
   * @param int $id_new
   *   Entity-id.
   * @command entity:change-id
   * @aliases ecid
   * @usage entity:change-id node 20 11
   */
  public function changeContentEntityId($entity_type_id, $id_old, $id_new): void
  {
    $this->entityIdManager->updateEntityId($entity_type_id, $id_old, $id_new);

    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entity = $storage->load($id_new);
    $label = $entity->label();
    $this->logger()->success("\"$label\" Id changed: $id_old -> $id_new");
  }


  /**
   * Adjust SQLs AUTO_INCR for an entity table
   *
   * @entity-type
   * @param string $entity_type_id
   *   Entity_type
   * @param int $value
   * @command entity:reset-next-id
   * @usage entity:reset-next-id
   */
  public function adjustAutoIncrement($entity_type_id, int $value): void
  {
    // TODO
    // $
    $this->entityIdManager->setAutoIncr($entity_type_id, $value);
  }
}
