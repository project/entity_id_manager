<?php

namespace Drupal\entity_id_manager;



use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;

/**
 * Service description.
 */
class EntityIdManager
{
  // @todo cache maps like EntityFieldManagerInterface does
  // use UseCacheBackendTrait;
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    private readonly Connection $connection,
    private readonly CacheBackendInterface $cacheEntity
  ) {}

  public function getEntityTypeFieldMap(): array
  {
    $map = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type instanceof ContentEntityTypeInterface) {
        $map[$entity_type_id] = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);
      }
    }

    return $map;
  }

  // @todo extend TableMappingInterface instead and put logic inside there
  public function getEntityReferenceTableMap(string $target_entity_type_id): array
  {
    $map = [];

    foreach ($this->getEntityTypeFieldMap() as $entity_type_id => $field_map) {
      foreach ($field_map as $field_id => $definition) {
        if (str_starts_with($definition->getType(), "entity_reference")) {
          // if (!in_array('entity', $definition->getPropertyNames())) {
          /** @var \Drupal\Core\TypedData\DataReferenceDefinitionInterface  $dataReferenceDefinition */
          $dataReferenceDefinition = $definition->getPropertyDefinition('entity');
          /** @var \Drupal\Core\Entity\TypedData\EntityDataDefinitionInterface $entityDataDefinition */
          $entityDataDefinition = $dataReferenceDefinition->getTargetDefinition();

          if ($entityDataDefinition->getEntityTypeId() == $target_entity_type_id) {
            $map[$entity_type_id][$field_id] = $definition;
          }
        }
      }
    }

    return $map;
  }

  public function getAvailableIds($entity_type_id, int $n_ids = 25) {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $ids = $storage->getQuery()->accessCheck(FALSE)->execute();

    $available_ids = [];
    $id = 1;
    while (count($available_ids) < $n_ids) {
      if (!key_exists($id, $ids)) {
        $available_ids[] = $id;
      }
      $id++;
    }

    return $available_ids;
  }

  public function updateEntityId(string $entity_type_id, int $id_old, int $id_new, bool $update_entity_references = TRUE)
  {
    /* @var Drupal\Core\Entity\Sql\SqlEntityStorageInterface */
    $storage = $this->entityTypeManager->getStorage($entity_type_id);

    /* @var Drupal\Core\Entity\Sql\TableMappingInterface $tableMapping */
    $tableMapping = $storage->getTableMapping();

    // $this->logger()->notice("Update tables of Entity itself");
    // @see https://drupal.stackexchange.com/questions/310610/how-do-i-get-the-names-of-the-tables-where-field-data-is-stored
    foreach ($tableMapping->getTableNames() as $table_name) {

      // @todo Use more $tableMapping methods to gain the right column_names
      $column_names = [
        $this->entityTypeManager->getDefinition($entity_type_id)->getKey('id'),
        'entity_id' // {$entity_type}__field_* tables name their id columns 'entity_id'
      ];

      foreach ($column_names as $column_name) {
        if (in_array($column_name, $tableMapping->getAllColumns($table_name))) {
          $this->updateTableEntityId($table_name, $column_name, $id_old, $id_new);
        }
      }
    }

    // Update Field Tables, referencing this Entity
    // $this->logger()->notice("Update tables of entity reference fields, referencing this Entity");
    if ($update_entity_references) {
      foreach ($this->getEntityReferenceTableMap($entity_type_id) as $entity_type_id => $field_map) {
        /** @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $storage */
        $storage = $this->entityTypeManager->getStorage($entity_type_id);
        $tableMapping = $storage->getTableMapping();

        foreach ($field_map as $field_id => $definition) {
          $column_name = $tableMapping->getFieldColumnName($definition, $definition->getMainPropertyName());
          foreach ($tableMapping->getAllFieldTableNames($field_id) as $table_name) {
            $this->updateTableEntityId($table_name, $column_name, $id_old, $id_new);
          }
        }
      }
    }

    // @optimize Be more precise with cache deletion -> Need something like $this->cacheEntity->delete('entity')
    $this->cacheEntity->deleteAll();
  }
  private function updateTableEntityId(string $table_name, string $column_name, int $id_old, int $id_new): string
  {
    $query = $this->connection->update($table_name)->condition($column_name, $id_old)->fields([$column_name => $id_new]);

    // $query_string = strval($query); // unreadable due to 'placeholders'
    // @todo how to substitue these placeholders with actual strings?

    $updated_rows = $query->execute();
    return "UPDATE $table_name SET $column_name = $id_new WHERE $column_name = $id_old - Rows affected: $updated_rows";
  }

  private function setAutoIncr($entity_type_id, int $value) {
    // @implement
    // $table_name = 'abcdefg';
    // $query = $this->connection->alter($table_name)->set('AUTO_INCREMENT', $value)->execute();
  }
}
